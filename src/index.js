import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import './index.css';

class App extends Component {
  render() {
    return (
      <>
        <div>Mobx Decorator Trainee</div>
      </>
    );
  }
}

ReactDOM.render(
  <React.StrictMode>
    <App/>
  </React.StrictMode>,
  document.getElementById('root')
);
